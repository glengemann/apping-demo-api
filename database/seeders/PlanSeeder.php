<?php

namespace Database\Seeders;

use App\Models\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    public function run(): void
    {
        Plan::insert([
            [
                'name' => 'Basic Plan',
                'description' => 'This is the basic plan. You won\'t have nothing.',
                'price' => 999,
            ],
            [
                'name' => 'Premium Plan',
                'description' => 'This is the premium plan. You\'ll have mugs, shirts and much more.',
                'price' => 1999,
            ],
            [
                'name' => 'Enterprise Plan',
                'description' => 'This is the enterprise plan. You\'ll have video tutorials.',
                'price' => 2999,
            ],
            [
                'name' => 'Enterprise Plus Plan',
                'description' => 'This is the enterprise plus plan. You\'ll have all plus pizza.',
                'price' => 3999,
            ],
        ]);
    }
}
