<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SubscriberSeeder extends Seeder
{
    public function run(): void
    {
        User::insert([
            [
                'name' => 'Karl Popper',
                'admin' => false,
                'credit_cart_number' => '4242424242424242',
                'email' => 'karl@popper.com',
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ],
            [
                'name' => 'Bertrand Russell',
                'admin' => false,
                'credit_cart_number' => '5555555555554444',
                'email' => 'bertrand@russell.com',
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ],
            [
                'name' => 'David Hume',
                'admin' => false,
                'credit_cart_number' => '4000000000000259',
                'email' => 'david@hume.com',
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ],
        ]);
    }
}
