<?php

namespace Database\Seeders;

use App\Models\Plan;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    private const BILLING_CYCLE = [
        'monthly' => '+1 month'
    ];

    public function run(): void
    {
        $users = User::select('id')->where('admin', false)
            ->get()
            ->modelKeys()

        ;

        $plans = Plan::all()
            ->modelKeys()
        ;

        for ($i = 0; $i < 15; $i++) {
            Subscription::insert([
                [
                    'user_id' => $users[array_rand($users)],
                    'plan_id' => $plans[array_rand($plans)],
                    'status' => 'active',
                    'start' => new \DateTimeImmutable(),
                    'end' => (new \DateTimeImmutable())->modify(self::BILLING_CYCLE['monthly']),
                ],
            ]);
        }

        for ($i = 0; $i < 5; $i++) {
            Subscription::insert([
                [
                    'user_id' => $users[array_rand($users)],
                    'plan_id' => $plans[array_rand($plans)],
                    'status' => 'active',
                    'start' => new \DateTimeImmutable(),
                    'end' => (new \DateTimeImmutable())->modify('-1 day'),
                ],
            ]);
        }
    }
}
