# Apping API - Subscription

## Set Up

- Clone the repository

```bash
git clone git@gitlab.com:glengemann/apping-demo-api.git
```

- In the folder created after clone the repository execute:

```bash
cd apping-demo-api
```

- Install the dependencies.

```bash
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
```

- Copy the environment file and edit conveniently.

```bash 
cp .env.example .env
```

- Run the containers using *sail*.

```bash
./vendor/bin/sail up
```

- Create the database schema.

```bash
./vendor/bin/sail artisan migrate
```

- Populate the database.

```bash
./vendor/bin/sail artisan db:seed
```

### 

## How to use

Let's go to create a subscription.

### First: Create a user

```bash
curl --location '127.0.0.1:8082/api/register' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-raw '{
    "name": "Francis Bacon",
    "email": "francis.bacon@gmail.com",
    "password": "password"
}' | jq
```

Check the response and grab the token for the subsequent request that needs authorization:

```json
{
    "message": "User created.",
    "token": "1|laravel_sanctum_KeZ5XT9jiXBriZb5tXgpD826nMvo165DwvglY7jzc3e75c8a"
}
```

### Second: List the available plans

```bash
curl --location '127.0.0.1:8082/api/plan' \
    --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer 1|laravel_sanctum_KeZ5XT9jiXBriZb5tXgpD826nMvo165DwvglY7jzc3e75c8a' | jq
```

From the response pick a plan:

```json
[
    {
        "id": 1,
        "name": "Basic Plan",
        "description": "This is the basic plan. You won't have nothing.",
        "price": 999,
        "created_at": null,
        "updated_at": null
    },
    {
        "id": 2,
        "name": "Premium Plan",
        "description": "This is the premium plan. You'll have mugs, shirts and much more.",
        "price": 1999,
        "created_at": null,
        "updated_at": null
    },
    {
        "id": 3,
        "name": "Enterprise Plan",
        "description": "This is the enterprise plan. You'll have video tutorials.",
        "price": 2999,
        "created_at": null,
        "updated_at": null
    }
]
```

### Third: Create a Subscription

```bash
curl --location '127.0.0.1:8082/api/subscription' \
    --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer 1|laravel_sanctum_KeZ5XT9jiXBriZb5tXgpD826nMvo165DwvglY7jzc3e75c8a' \
    --data-raw '{
        "email": "francis.bacon@gmail.com",
        "plan": "Basic Plan",
        "credit_cart_number": "4242424242424242"
    }' | jq
```

If the subscription was created, the API will return the subscription details:

```json
{
    "user_id": 1,
    "plan_id": 1,
    "status": "active",
    "start": {
        "date": "2023-09-03 18:43:36.847324",
        "timezone_type": 3,
        "timezone": "UTC"
    },
    "end": {
        "date": "2023-10-03 18:43:36.847325",
        "timezone_type": 3,
        "timezone": "UTC"
    },
    "updated_at": "2023-09-03T18:43:36.000000Z",
    "created_at": "2023-09-03T18:43:36.000000Z",
    "id": 1
}
```

## Perform Admin Actions

We have a default admin user out of the box. In order to access the URI restricted to the
admin user, we can follow these steps.

### First: Login

```bash
curl --location '127.0.0.1:8082/api/login' \
    --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --data-raw '{
        "email": "jhon@lock.com",
        "password": "password"
    }' | jq
```

Look at the token, this tokes will have all the abilities.

```json
{
    "token": "3|laravel_sanctum_O4RA3eKI7zKJRydxJJnqkbkGGic4hazMnbec7nVjeb3b783a"
}
```

## Second: Request the protected url

```bash
curl --location '127.0.0.1:8082/api/report/payment' \
    --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer 3|laravel_sanctum_O4RA3eKI7zKJRydxJJnqkbkGGic4hazMnbec7nVjeb3b783a' \
    | jq
```

## Maturity Model

- Database design :ballot_box_with_check:, we offer a minimalistic database model with three tables: `users`, `plans`,
  and `subscriptions`.
- API design :ballot_box_with_check:, we offer at least fourth endpoints groups: `user/subscriber`,
  `plan`, `subscription`, and `report`.
- Unit Test :ballot_box_with_check:, we offer several test cases, unit and integration tests.
- Documentation :ballot_box_with_check:.
- GIT :ballot_box_with_check:.
- JWT/sanctum :ballot_box_with_check:.
- PHP :ballot_box_with_check:. We use the latest version of the language 8.2, and some of the most recent
  characteristics.
- Emails :ballot_box_with_check:, we offer two email classes.
- Jobs :ballot_box_with_check:, The `SendWelcomeEmailJob` was created to send the welcome message as an async task,
  and you must supply the convenient configuration.
- Commands :ballot_box_with_check:, we offer a command that can be added as and cron job to
  process the expired subscriptions.
- Middleware :ballot_box_with_check:, we use the `sanctum` middleware to protect some URIs. Also, we use the
  `ability` and `abilities` out of the box middlewares to perform authorization validations.
- Validations :ballot_box_with_check:, we offer validation for some incoming request.

## Endpoints

- Plan
- Subscription
- Subscriber
- Report

### Plan

| Verb      | URI             | authorization |
|-----------|-----------------|---------------|
| GET/HEAD  | api/plan        | all           |
| POST      | api/plan        | admin         |
| GET/HEAD  | api/plan/{plan} | all           |
| PUT/PATCH | api/plan/{plan} | admin         |
| DELETE    | api/plan/{plan} | admin         |

### Subscription

| Verb      | URI                             | authorization |
|-----------|---------------------------------|---------------|
| GET/HEAD  | api/subscription                | admin         |
| POST      | api/subscription/               | all           |
| GET/HEAD  | api/subscription/{subscription} | admin,self    |
| PUT/PATCH | api/subscription/{subscription} | admin,self    | 
| DELETE    | api/subscription/{subscription} | admin         |

#### Examples

- Get Subscription

```bash
curl --location '127.0.0.1:8082/api/subscription' \
    --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer 6|laravel_sanctum_HW4ZxodekncwNDEZ7KAJPQ4AB6ttOFlHCDaAoOYVf7d359aa' | jq
```

### Subscriber

| Verb      | URI                         | authorization |
|-----------|-----------------------------|---------------|
| POST      | api/login                   | all           |
| POST      | api/register                | all           |
| GET/HEAD  | api/subscriber              | admin         |
| GET/HEAD  | api/subscriber/{subscriber} | admin         |
| PUT/PATCH | api/subscriber/{subscriber} | admin         |
| DELETE    | api/subscriber/{subscriber} | admin         |

### Report

| Verb     | URI                     | authorization |
|----------|-------------------------|---------------|
| GET/HEAD | api/report/payment      | admin         |
| GET/HEAD | api/report/popular-plan | admin         |
| GET/HEAD | api/report/popular-user | admin         |
| GET/HEAD | api/report/subscription | admin         |

#### Examples

- The most popular subscriptions

```bash
curl --location '127.0.0.1:8082/api/report/popular-plan' \
    --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer 3|laravel_sanctum_O4RA3eKI7zKJRydxJJnqkbkGGic4hazMnbec7nVjeb3b783a' \
    | jq
```

- The user with more subscriptions

```bash
curl --location '127.0.0.1:8082/api/report/popular-user' \
    --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer 1|laravel_sanctum_fwkj3IL5zueBabvHDoB0q5sgIvjQ2P7bfU9Wop4K73d2ea58' \
    | jq
```

## Database Model

```
user
    id - integer
    name - string
    admin - bool
    email - string
```

```
plan
    id - integer
    name - string
    description - text
    price - integer
```

```
subscriptions
    id - integer
    user_id - integer
    role_id - integer 
    status - string (active, cancelled, expired)
    start - date
    end - date
```

### Entities

- *Plan* entity is going to store the plan name, description, and price.
- *User*

# Artisan History

## Plan

```
./vendor/bin/sail artisan make:model Plan -mfs
./vendor/bin/sail artisan make:controller PlanController --api --model=Plan
./vendor/bin/sail artisan make:resource PlanResource
./vendor/bin/sail artisan make:resource PlanCollection

./vendor/bin/sail artisan migrate --pretend
./vendor/bin/sail artisan migrate
```

## User

```
./vendor/bin/sail artisan make:controller UserController --api --model=User
./vendor/bin/sail artisan make:resource UserResource
./vendor/bin/sail artisan make:resource UserCollection
```

## Subscription

```
./vendor/bin/sail artisan make:model Subscription -mfs
./vendor/bin/sail artisan make:controller SubscritionController --api
./vendor/bin/sail artisan make:resource SubscriptionResource
./vendor/bin/sail artisan make:resource SubscriptionCollection
```

## Command

```
./vendor/bin/sail artisan make:command ProcessPayment
./vendor/bin/sail artisan app:process-payment
```

## Job

```
./vendor/bin/sail artisan make:job SendWelcomeEmailJob
```

## Database

```
./vendor/bin/sail artisan db:wipe
./vendor/bin/sail artisan migrate --seed
./vendor/bin/sail artisan test
```

## References

- https://stripe.com/docs/testing
