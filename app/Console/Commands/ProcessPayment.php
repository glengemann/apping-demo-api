<?php

namespace App\Console\Commands;

use App\Models\Subscription;
use App\Models\User;
use App\Services\PaymentProcessing;
use App\Services\SubscriptionEngine;
use Illuminate\Console\Command;

class ProcessPayment extends Command
{
    private const BILLING_CYCLE = [
        'monthly' => '+1 month'
    ];
    protected $signature = 'app:process-payment';

    protected $description = 'Process payments.';

    public function handle(PaymentProcessing $paymentProcessing, SubscriptionEngine $subscriptionEngine): void
    {
        $subscriptions = Subscription::all();
        $today = new \DateTimeImmutable();
        $this->info($this->precessDescription($today));
        $subscriptions
            ->each(function ($subscription) use ($subscriptionEngine, $today, $paymentProcessing) {
                $this->renewSubscription($subscription, $today, $paymentProcessing, $subscriptionEngine);
            })
        ;
    }

    private function precessDescription(\DateTimeImmutable $today): string
    {
        $msg = <<<MESSAGE
        Today is %s. We are going to renew all subscriptions that expired today or those that have already expired.
        MESSAGE;

        return sprintf(
            $msg,
            $today->format('Y-m-d')
        );
    }

    private function subscriptionExpired($subscription): string
    {
        return <<<MESSAGE
        The subscription #$subscription->id that expired $subscription->end will be renew.
        MESSAGE;
    }

    function renewSubscription(
        Subscription $subscription,
        \DateTimeImmutable $today,
        PaymentProcessing $paymentProcessing,
        SubscriptionEngine $subscriptionEngine
    ): void {
        if ($subscriptionEngine->isExpired($subscription)) {
            $user = User::find($subscription->user_id);
            $this->warn($this->subscriptionExpired($subscription));

            $payment = $paymentProcessing->process($user);

            if ($payment) {
                $subscription->end = $today->modify(self::BILLING_CYCLE['monthly']);
                $subscription->save();
                $this->info("The subscription #{$subscription->id} was renew.");
            } else {
                $this->error('Card decline');
            }
        }
    }
}
