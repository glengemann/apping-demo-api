<?php

namespace App\Services;

use App\Models\Subscription;

class SubscriptionEngine
{
    private \DateTimeImmutable $today;
    public function __construct()
    {
        $this->today = new \DateTimeImmutable();
    }

    public function canCancelled(Subscription $subscription): bool
    {
        return $this->getToday() < new \DateTimeImmutable($subscription->end);
    }

    public function isExpired(Subscription $subscription): bool
    {
        return $this->getToday() >= new \DateTimeImmutable($subscription->end);
    }

    public function getToday(): \DateTimeImmutable
    {
        return $this->today;
    }
}
