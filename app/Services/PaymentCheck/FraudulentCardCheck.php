<?php

namespace App\Services\PaymentCheck;

use App\Models\User;

class FraudulentCardCheck implements PaymentCheck
{

    public function validate(User $user): bool
    {
        return !in_array($user->credit_cart_number, $this->getFraudulentCards());
    }

    public function getFraudulentCards(): array
    {
        return [
            '4000000000000259',
            '4000000000002685',
            '4000000000001976'
        ];
    }
}
