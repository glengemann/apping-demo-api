<?php

namespace App\Services\PaymentCheck;

use App\Models\User;

interface PaymentCheck
{
    public function validate(User $user): bool;
}
