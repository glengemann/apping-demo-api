<?php

namespace App\Services\PaymentCheck;

use App\Models\User;

class DenyListCheck implements PaymentCheck
{

    public function validate(User $user): bool
    {
        return !in_array($user->email, $this->getDenyList());
    }

    private function getDenyList(): array
    {
        return [
            'foo@bar.com',
            'bar@foo.com',
        ];
    }
}
