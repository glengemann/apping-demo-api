<?php

namespace App\Services\PaymentCheck;

use App\Models\User;

class CreditCardLength implements PaymentCheck
{

    public function validate(User $user): bool
    {
        return strlen($user->credit_cart_number) == 16;
    }
}
