<?php

namespace App\Services;

use App\Models\User;
use App\Services\PaymentCheck\CreditCardLength;
use App\Services\PaymentCheck\DenyListCheck;
use App\Services\PaymentCheck\FraudulentCardCheck;
use App\Services\PaymentCheck\PaymentCheck;

class PaymentProcessing
{
    /** @var PaymentCheck[] */
    private array $checkers;

    public function __construct()
    {
        $this->checkers = [
            new CreditCardLength(),
            new DenyListCheck(),
            new FraudulentCardCheck(),
        ];
    }

    public function process(User $user): bool
    {
        $result = [];
        foreach ($this->checkers as $checker) {
            $result[] = $checker->validate($user);
        }

        return $this->success($result);
    }

    public function success(array $result): bool
    {
        return array_product($result);
    }
}
