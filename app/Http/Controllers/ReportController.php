<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function payment(): JsonResponse
    {
        $report = Subscription
            ::select(
                DB::raw('SUM(end <= NOW()) as pending'),
                DB::raw('SUM(end > NOW()) as processed')
            )
            ->get()
        ;

        $total = Subscription::count();

        $report = [
            ...$report->toArray()[0],
            'total' => $total,
        ];

        return response()->json($report, Response::HTTP_OK);
    }

    public function subscription(): JsonResponse
    {
        $report = Subscription::select('status', DB::raw('COUNT(*) as count'))
            ->groupBy('status')
            ->get()
        ;
        $total = Subscription::count();

        $report = $report->reduce(function ($carry, $column) {
            $flat[$column['status']] = $column['count'];
            return $carry + $flat;
        }, []);

        $report = [
            ...$report,
            'total' => $total,
        ];

        return response()->json($report, Response::HTTP_OK);
    }

    public function popularPlan()
    {
        $report = Subscription::select('plan_id', DB::raw('COUNT(*) as plan_count'))
            ->groupBy('plan_id')
            ->limit(10)
            ->get()
        ;

        $report = $report->reduce(function ($carry, $record) {
            $flat[$record['plan_id']] = $record['plan_count'];
            return $carry + $flat;
        }, []);

        return response()->json($report, Response::HTTP_OK);
    }

    public function popularUser()
    {
        $report = Subscription
            ::select('user_id', DB::raw('COUNT(*) as count'))
            ->groupBy('user_id')
            ->orderByDesc('count')
            ->limit(5)
            ->get()
        ;

        $report = $report->reduce(function ($carry, $record) {
           return $carry + [$record['user_id'] => $record['count']];
        }, []);

        return response()->json($report, Response::HTTP_OK);
    }
}
