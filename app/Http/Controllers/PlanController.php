<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlanCollection;
use App\Http\Resources\PlanResource;
use App\Models\Plan;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PlanController extends Controller
{
    public function index(): JsonResponse
    {
        $plans = Plan::all();

        return response()
            ->json(new PlanCollection($plans), Response::HTTP_OK);
    }

    public function store(Request $request): JsonResponse
    {
        $data = $request->only(['name', 'description', 'price']);

        $plan = Plan::create($data);

        return response()
            ->json(new PlanResource($plan), Response::HTTP_OK);
    }

    public function show(Plan $plan): JsonResponse
    {
        return response()->json($plan, Response::HTTP_OK);
    }

    public function update(Request $request, Plan $plan): JsonResponse
    {
        $data = $request->only(['name', 'description', 'price']);
        $plan->update($data);

        return response()
            ->json(new PlanResource($plan), Response::HTTP_OK);
    }

    public function destroy(Plan $plan): JsonResponse
    {
        $plan->delete();

        return response()->json([], Response::HTTP_NO_CONTENT);
    }


}
