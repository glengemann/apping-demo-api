<?php

namespace App\Http\Controllers;

use App\Http\Resources\SubscriberCollection;
use App\Http\Resources\SubscriberResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class SubscriberController extends Controller
{
    public function index()
    {
        $subscribers = User::where('admin', false)
            ->get();

        return response()
            ->json(new SubscriberCollection($subscribers), Response::HTTP_OK);
    }

    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
            ]
        );

        if ($validator->fails()) {
            $data = [
                $validator->errors(),
            ];
            return response()->json($data, Response::HTTP_UNAUTHORIZED);
        }

        $password = $validator->safe(['password']);
        $password = array_map(fn($item) => Hash::make($item), $password);
        $user = $validator->safe()->except('password') + $password;
        $user = User::create($user);

        return response()->json([
            'message' => 'User created.',
            'token' => $user->createToken('subscriber', ['subscriber:manage'])->plainTextToken
        ], Response::HTTP_OK);
    }

    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|exists:users',
                'password' => 'required',
            ]
        );

        if ($validator->fails()) {
            $data = [
                $validator->errors(),
            ];
            return response()->json($data, Response::HTTP_UNAUTHORIZED);
        }

        $credentials = $validator->validated();
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = match ((bool) $user->admin) {
                false => $user->createToken('subscriber', ['subscriber:manage']),
                true => $user->createToken('admin'),
            };

            return response()->
                json(['token' => $token->plainTextToken], Response::HTTP_OK);
        }

        return response()
            ->json(['Invalid login credentials'], Response::HTTP_UNAUTHORIZED);
    }

    public function show(User $user): JsonResponse
    {
        $subscriber = $user->admin ? new User() : $user;

        return response()
            ->json(new SubscriberResource($subscriber), Response::HTTP_OK);
    }

    public function update(Request $request, User $user)
    {
        if ($user->admin) {
            return response()
                ->json(['message' => 'You are not a subscriber.'], Response::HTTP_OK);
        }

        $data = $request->only(['name','email','credit_cart_number']);
        $user->update($data);

        return response()
            ->json(new SubscriberResource($user), Response::HTTP_OK);
    }

    public function destroy(User $user)
    {
        if ($user->admin) {
            return response()
                ->json(['message' => 'The admin cannot be deleted.'], Response::HTTP_OK);
        }

        $user->delete();

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
