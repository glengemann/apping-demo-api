<?php

namespace App\Http\Controllers;

use App\Http\Resources\SubscriptionCollection;
use App\Http\Resources\SubscriptionResource;
use App\Jobs\SendWelcomeEmailJob;
use App\Mail\SubscriptionCancelledMail;
use App\Models\Plan;
use App\Models\Subscription;
use App\Models\User;
use App\Services\PaymentProcessing;
use App\Services\SubscriptionEngine;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SubscriptionController extends Controller
{
    private const BILLING_CYCLE = [
        'monthly' => '+1 month'
    ];

    public function index(): JsonResponse
    {
        $subscriptions = Subscription::all();

        return response()
            ->json(new SubscriptionCollection($subscriptions), Response::HTTP_OK);
    }

    public function store(Request $request, PaymentProcessing $paymentProcessing): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|exists:users,email',
                'plan' => 'required|string|exists:plans,name',
                'credit_cart_number' => 'required|numeric',
            ],
        );

        if ($validator->fails()) {
            if ($validator->fails()) {
                $data = [
                    $validator->errors(),
                ];
                return response()->json($data, Response::HTTP_BAD_REQUEST);
            }
        }

        $user = User::where('email', $validator->safe(['email']))->first();
        $creditCardNumber = $request->get('credit_cart_number');
        $user->credit_cart_number = $creditCardNumber;

        $process = $paymentProcessing->process($user);

        if (!$process) {
            return response()
                ->json(['message' => 'Card decline'], Response::HTTP_OK);
        }

        $user->save();

        $plan = Plan::where('name', $validator->safe(['name']))->first();

        $data = [
            'user_id' => $user->id,
            'plan_id' => $plan->id,
            'status' => 'active',
            'start' => new \DateTimeImmutable(),
            'end' => (new \DateTimeImmutable())->modify(self::BILLING_CYCLE['monthly']),
        ];

        $subscription = Subscription::create($data);

        SendWelcomeEmailJob::dispatch($user, $subscription);

        return response()
            ->json(new SubscriptionResource($subscription), Response::HTTP_OK);
    }

    public function show(Subscription $subscription): JsonResponse
    {
        return response()->json(new SubscriptionResource($subscription), Response::HTTP_OK);
    }

    public function update(Request $request, Subscription $subscription): JsonResponse
    {
        $data = $request->only(['status', 'end']);

        $subscription->update($data);

        return response()
            ->json(new SubscriptionResource($subscription, Response::HTTP_OK));
    }

    public function cancel(Subscription $subscription, SubscriptionEngine $subscriptionEngine)
    {
        if (!$subscriptionEngine->canCancelled($subscription)) {
            $msg = 'It is not possible to cancel the subscription. Some of our cron job is or will be precessing the pay.';
            return response()->json(['message' => $msg], Response::HTTP_OK);
        }

        $subscription->update(['status' => 'cancelled']);

        Mail::to(User::find($subscription->user_id))
            ->send(new SubscriptionCancelledMail($subscription));

        return redirect('/')
            ->with('status', 'Subscription canceled!');
    }

    public function destroy(Subscription $subscription): JsonResponse
    {
        $subscription->delete();

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
