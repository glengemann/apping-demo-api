<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SubscriberResource extends JsonResource
{
    /*
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'admin' => $this->admin,
            'credit_cart_number' => $this->credit_cart_number,
            'email' => $this->email,
            'email_verified_at' => $this->email_verified_at,
            'subscriptions' => $this->subscriptions->map(function ($subscription) {
                return [
                    'id' => $subscription->id,
                    'name' => $subscription->name,
                    'description' => $subscription->description,
                    'price' => $subscription->price,
                ];
            }),
        ];
    }
}
