<?php

namespace Tests\Unit\Services\PaymentCheck;

use App\Models\User;
use App\Services\PaymentProcessing;
use App\Services\PaymentCheck\CreditCardLength;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class CreditCardLengthTest extends TestCase
{
    public static function cardsProvider(): array
    {
        return [
            ['4242424242424242', true], // visa
            ['5555555555554444', true], // mastercard
            ['123', false],
        ];
    }

    #[DataProvider('cardsProvider')]
    public function testValidate(string $creditCard, bool $expected): void
    {
        $user = new User(['credit_cart_number' => $creditCard]);

        $validator = new CreditCardLength();
        $actual = $validator->validate($user);

        $this->assertEquals($expected, $actual);
    }
}
