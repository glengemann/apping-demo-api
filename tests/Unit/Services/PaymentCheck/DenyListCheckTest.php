<?php

namespace Tests\Unit\Services\PaymentCheck;

use App\Models\User;
use App\Services\PaymentCheck\DenyListCheck;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

class DenyListCheckTest extends TestCase
{
    public static function cardsProvider(): array
    {
        return [
            ['foo@bar.com', false],
            ['ooo@fff', true],
        ];
    }

    #[DataProvider('cardsProvider')]
    public function testValidate(string $email, bool $expected)
    {
        $user = new User(['email' => $email]);

        $validator = new DenyListCheck();
        $actual = $validator->validate($user);

        $this->assertEquals($expected, $actual);
    }
}
