<?php

namespace Tests\Unit\Services\PaymentCheck;

use App\Models\User;
use App\Services\PaymentCheck\FraudulentCardCheck;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

class FraudulentCardCheckTest extends TestCase
{
    public static function cardsProvider(): array
    {
        return [
            ['4000000000000259', false],
            ['5555555555554444', true],
        ];
    }

    #[DataProvider('cardsProvider')]
    public function testValidate(string $creditCard, bool $expected): void
    {
        $user = new User(['credit_cart_number' => $creditCard]);

        $validator = new FraudulentCardCheck();
        $actual = $validator->validate($user);

        $this->assertEquals($expected, $actual);
    }
}
