<?php

namespace Tests\Services;

use App\Models\Subscription;
use App\Services\SubscriptionEngine;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class SubscriptionEngineTest extends TestCase
{
    public static function subscriptionProvider(): array
    {
        return [
            [new Subscription(['end' => '2023-08-14']), false],
            [new Subscription(['end' => '2023-08-15']), false],
            [new Subscription(['end' => '2023-08-16']), true],
        ];
    }

    #[DataProvider('subscriptionProvider')]
    public function testCanCancelled(Subscription $subscription, bool $expected): void
    {
        $mock = $this->getMockBuilder(SubscriptionEngine::class)
            ->onlyMethods(['getToday'])
            ->getMock()
        ;

        $today = new \DateTimeImmutable('2023-08-15');
        $mock->expects($this->any())
            ->method('getToday')
            ->willReturn($today)
        ;

        $actual = $mock->canCancelled($subscription);
        $this->assertEquals($expected, $actual);
    }

    #[DataProvider('subscriptionProvider')]
    public function testIsExpired(Subscription $subscription, bool $expected): void
    {
        $mock = $this->getMockBuilder(SubscriptionEngine::class)
            ->onlyMethods(['getToday'])
            ->getMock()
        ;

        $today = new \DateTimeImmutable('2023-08-15');
        $mock->expects($this->any())
            ->method('getToday')
            ->willReturn($today)
        ;

        $actual = $mock->isExpired($subscription);

        $this->assertEquals(!$expected, $actual);
    }
}
