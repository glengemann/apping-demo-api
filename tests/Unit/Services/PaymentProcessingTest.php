<?php

namespace Tests\Unit\Services;

use App\Models\User;
use App\Services\PaymentProcessing;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class PaymentProcessingTest extends TestCase
{
    public static function cardsProvider(): array
    {
        return [
            [
                new User([
                    'credit_cart_number' => '4242424242424242',
                    'email' => 'karl@popper.com'
                ]),
                true
            ],
            [
                new User([
                    'credit_cart_number' => '4000000000000259',
                    'email' => 'karl@popper.com'
                ]),
                false
            ],
            [
                new User([
                    'credit_cart_number' => '4242424242424242',
                    'email' => 'foo@bar.com'
                ]),
                false
            ],
            [
                new User([
                    'credit_cart_number' => '4000000',
                    'email' => 'karl@popper.com'
                ]),
                false
            ],
        ];
    }

    #[DataProvider('cardsProvider')]
    public function testProcess(User $user, bool $expected): void
    {
        $engine = new PaymentProcessing();
        $actual = $engine->process($user);

        $this->assertEquals($expected, $actual);
    }

    public function testSuccess()
    {
        $processor = new PaymentProcessing();

        $this->assertTrue($processor->success([1,1,1]));
        $this->assertFalse($processor->success([0,0,0]));
        $this->assertFalse($processor->success([1,0,0]));
        $this->assertFalse($processor->success([0,1,0]));
    }
}
