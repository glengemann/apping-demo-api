<?php

namespace Tests\Feature\Http\Controllers;

use Database\Seeders\AdminSeeder;
use Database\Seeders\SubscriberSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class SubscriberControllerTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;

    public function testSubscriberIndex(): void
    {
        $this->seed(SubscriberSeeder::class);

        $response = $this->get('api/subscriber');

        $response->assertStatus(200);
        $this->assertCount(3, $response->json());
    }

    public function testSubscriberStoreBodyEmpty()
    {
        $response = $this->post('/api/register');

        $response->assertStatus(401);
        $this->assertCount(3, $response->json()[0]);
    }

    public function testSubscriberStoreSuccess()
    {
        $response = $this->post(
            '/api/register',
            [
                "name" => "Immanuel Kant",
                "email" => "immanuel@kant.com",
                "password" => "password",
            ],
        );

        $response->assertStatus(200);
        $this->assertCount(2, $response->json());
    }

    public function testLoginWithAdminUserUnauthorized()
    {
        $this->seed(AdminSeeder::class);

        $response = $this->post(
            '/api/login',
            [
                'email' => 'jhon@lock.com',
                'password' => 'wrongpassword',
            ],
        );

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        $this->assertCount(1, $response->json());
    }

    public function testLoginWithAdminUser(): void
    {
        $this->seed(AdminSeeder::class);

        $response = $this->post(
            '/api/login',
            [
                'email' => 'jhon@lock.com',
                'password' => 'password',
            ],
        );

        $response->assertStatus(Response::HTTP_OK);
        $this->assertCount(1, $response->json());
    }
}
