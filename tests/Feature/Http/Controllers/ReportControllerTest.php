<?php

namespace Tests\Feature\Http\Controllers;

use Database\Seeders\PlanSeeder;
use Database\Seeders\SubscriberSeeder;
use Database\Seeders\SubscriptionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ReportControllerTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;

    public function testSubscriberIndex(): void
    {
        $this->seed(SubscriberSeeder::class);
        $this->seed(PlanSeeder::class);
        $this->seed(SubscriptionSeeder::class);

        $response = $this->get('api/report/subscription');

        $response->assertStatus(200);
        $this->assertCount(2, $response->json());
    }
}
