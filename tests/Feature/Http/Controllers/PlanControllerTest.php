<?php

namespace Tests\Feature\Http\Controllers;

use Database\Seeders\PlanSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PlanControllerTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;

    public function testSubscriberIndex(): void
    {
        $this->seed(PlanSeeder::class);

        $response = $this->get('api/plan');

        $response->assertStatus(200);
        $this->assertCount(4, $response->json());
    }
}
