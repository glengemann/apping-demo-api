<?php

namespace Tests\Feature\Http\Controllers;

use Database\Seeders\PlanSeeder;
use Database\Seeders\SubscriberSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class SubscriptionControllerTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;

    public function testSubscriptionIndex(): void
    {
        $response = $this->get('api/subscription');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testSubscriptionStoreBodyEmpty(): void
    {
        $response = $this->post('/api/subscription');

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
        $this->assertCount(3, $response->json()[0]);

        $expected = ['The email field is required.'];
        $this->assertEquals($expected, $response->json()[0]['email']);
    }

    public function testSubscriptionStoreInvalidCreditCard(): void
    {
        $this->seed(SubscriberSeeder::class);
        $this->seed(PlanSeeder::class);

        $response = $this->post(
            '/api/subscription',
            [
                'email' => 'karl@popper.com',
                'plan' => 'Basic Plan',
                'credit_cart_number' => '4242',
            ]
        );

        $response->assertStatus(Response::HTTP_OK);
        $expected = ['message' => 'Card decline'];
        $this->assertEquals($expected, $response->json());
    }

    public function testSubscriptionStoreSuccess(): void
    {
        $this->seed(SubscriberSeeder::class);
        $this->seed(PlanSeeder::class);

        $response = $this->post(
            '/api/subscription',
            [
                'email' => 'karl@popper.com',
                'plan' => 'Basic Plan',
                'credit_cart_number' => '4242424242424242',
            ]
        );

        $response->assertStatus(Response::HTTP_OK);

        $this->assertArrayHasKey('status', $response->json());
        $this->assertContains('active', $response->json());
    }
}
