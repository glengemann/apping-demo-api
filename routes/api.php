<?php

use App\Http\Controllers\PlanController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\SubscriberController;
use Illuminate\Support\Facades\Route;

/** Authentication */
Route::post('/register', [SubscriberController::class, 'store'])
    ->name('subscriber.register');
Route::post('/login', [SubscriberController::class, 'login'])
    ->name('subscriber.login');

/** User/Subscriber */
Route::apiResource('/subscriber', SubscriberController::class)
    ->only(['index','show','update','destroy'])
    ->middleware('auth:sanctum');

/** Plan */
Route::apiResource('/plan', PlanController::class)
    ->middleware('auth:sanctum');

/** Subscription */
Route::apiResource('/subscription', SubscriptionController::class)
    ->middleware(['auth:sanctum', 'ability:subscriber:manage']);

Route::get('/subscription/{subscription}/cancel', [SubscriptionController::class, 'cancel'])
    ->name('subscription.cancel');

/** Reports */
Route::get('/report/payment', [ReportController::class, 'payment'])
    ->name('report.payment')
    ->middleware(['auth:sanctum', 'ability:report'])
;

Route::get('/report/subscription', [ReportController::class, 'subscription'])
    ->name('report.subscription')
    ->middleware(['auth:sanctum', 'ability:report'])
;

Route::get('/report/popular-plan', [ReportController::class, 'popularPlan'])
    ->name('report.popular-plan')
    ->middleware(['auth:sanctum', 'ability:report'])
;

Route::get('/report/popular-user', [ReportController::class, 'popularUser'])
    ->name('report.popular-user')
    ->middleware(['auth:sanctum', 'ability:report'])
;
