<h1>Welcome!</h1>

<p>
    Your subscription is active.<br>

    <ul>
        <li>Name: {{ $subscription->user_id }}</li>
        <li>Plan: {{ $subscription->plan_id }}</li>
        <li>Status: {{ $subscription->status }}</li>
        <li>Start: {{ $subscription->start }}</li>
        <li>End: {{ $subscription->end }}</li>
    </ul>
</p>

<p>
    You can cancel the subscription
    <a href="{{ route('subscription.cancel', ['subscription' => $subscription]) }}">here</a>.
</p>
