<h1>Subscription Cancelled!</h1>

<p>
    Your subscription has been cancelled.<br>

    <ul>
        <li>Name: {{ $subscription->user_id }}</li>
        <li>Plan: {{ $subscription->plan_id }}</li>
        <li>Status: {{ $subscription->status }}</li>
        <li>Start: {{ $subscription->start }}</li>
        <li>End: {{ $subscription->end }}</li>
    </ul>
</p>
